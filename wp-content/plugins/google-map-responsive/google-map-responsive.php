<?php
/**
* Plugin Name: Google Map Responsive
* Plugin URI: https://www.interlective.com/
* Description: This is the used to make your loaction map resposively.
* Version: 1.0
* Author: B H S Sandaruwan
* Author URI: http://interlective.com/
**/


global $_getmapurl;
add_action('admin_menu','admin_menu_option');

function admin_menu_option(){
add_menu_page('Google Map Responsive', 'Google Map Responsive','manage_options','admin_menu','map_info','',200);
}

function map_info(){
   
if(array_key_exists('submit-data',$_POST)){
update_option('map_responsive_info',$_POST['map_url']);
}

$_getmapurl = get_option('map_responsive_info','');
?>
<div class="wrap">
    <h2>Google Map Responsive</h2>
    <p class="description">
        <label for="wpcf7-shortcode">Copy this shortcode and paste it into your post, page, or text widget
            content:</label>
        <span class="shortcode"><input type="text" id="wpcf7-shortcode" onfocus="this.select();" readonly="readonly"
                class="large-text code" value="[responsive_google_map]"></span>
    </p>
    <form action="" method="POST">
        <label for="text1">Enter Your Map Location Src</label>

        <textarea name="map_url" id="" class="large-text" cols="" rows="3"><?php print $_getmapurl; ?></textarea>
        <input type="submit" name="submit-data" class="button button-primary">
    </form>
</div>
<?php

}

add_action('wp_enqueue_scripts', 'callback_for_setting_up_scripts');
function callback_for_setting_up_scripts() {
    wp_register_style( 'google-map-responsive',  plugins_url( 'css/style-map.css', __FILE__ ) );
    wp_enqueue_style( 'google-map-responsive' );
    // wp_enqueue_script( 'namespaceformyscript', 'http://locationofscript.com/myscript.js', array( 'jquery' ) );
}
function responsive_map(){

    $_content  ="<div id='map-container-google-1' class='z-depth-1-half map-container' style='height: 100%'>";
    $_content .="<iframe src='". get_option('map_responsive_info','') ."' frameborder='0' height='400' style='border:0' allowfullscreen></iframe>";
$_content .="</div>";


return $_content;
}
add_shortcode('responsive_google_map','responsive_map');