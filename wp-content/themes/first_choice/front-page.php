<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();

$container = get_theme_mod( 'understrap_container_type' );

?>

<div class="bg-gradiant">
    <div class="container pb-5">
        <div class="row">
            <div class="col-md-8">
                <div class="slider-head">
                    Get the best deal<br />on your travel<br />money
                </div>
                <div class="slider-head-sub">
                    Most convenient way to order your travel money
                </div>
                <div class="section-icon">
                    <div class="section-icon-item">
                        <div class="icon-item">
                            <img src="<?php bloginfo("template_directory"); ?>/img/icon-click.png" alt="">
                        </div>
                        <div class="icon-text">
                            <div class="icon-item-head">
                                One Click Order
                            </div>
                            <div class="icon-content">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                Ipsum
                                has been the industry's standard dummy text ever since
                            </div>
                        </div>
                    </div>
                    <div class="section-icon-item mt-4">
                        <div class="icon-item">
                            <img src="<?php bloginfo("template_directory"); ?>/img/icon-delivery.png" alt="">
                        </div>
                        <div class="icon-text">
                            <div class="icon-item-head">
                                Fast Delivery
                            </div>
                            <div class="icon-content">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                Ipsum
                                has been the industry's standard dummy text ever since
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-4">
                <div class="exchenge-wrapper">

                    <?php
                echo do_shortcode('[currency_calculator]');
                ?>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="content-section section-padding">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="header-section text-center pt-4">
                    <h2>
                        Find the best Rates on the Market
                    </h2>
                </div>
                <div class="sub-heading-content text-center">
                    <p>
                        Enjoy the most competitive exchange rates in the market when you<br /> buy/sell foreign currency
                        with
                        First Choice
                    </p>
                </div>
                <div class="ex-featued col-md-6 mx-auto">
                    <?php
                    echo do_shortcode('[currency_list_featured]');
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="currency-section section-padding">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="header-section text-center pt-4">
                    <h2>
                        It’s an easy 1-2-3 steps to order<br />
                        your currencies online
                    </h2>
                </div>
                <div class="sub-heading-content text-center">
                    <p>
                        Enjoy the most competitive exchange rates in the market when you<br /> buy/sell foreign currency
                        with
                        First Choice
                    </p>
                </div>
            </div>


        </div>
        <div class="row padding-section-tb">
            <div class="col-md-4">
                <div class="text-center ">
                    <div class="currency-sec-img">
                        <img src="<?php bloginfo("template_directory"); ?>/img/currency-1.png" alt="" class="img-fluid">
                    </div>
                    <div class="currency-sec-head">
                        Choose your Currency & Amount
                    </div>
                    <div class="currency-sec-text">
                        <p> Choose your currency and enter the amount. You can even order multiple currencies in a
                            single
                            order
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="text-center ">
                    <div class="currency-sec-img">
                        <img src="<?php bloginfo("template_directory"); ?>/img/payment.png" alt="" class="img-fluid">
                    </div>
                    <div class="currency-sec-head">
                        Choose Payment & Shipping Method
                    </div>
                    <div class="currency-sec-text">
                        <p> Choose how you want your money delivered and the method you would like to pay
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="text-center ">
                    <div class="currency-sec-img">
                        <img src="<?php bloginfo("template_directory"); ?>/img/confirm.png" alt="" class="img-fluid">
                    </div>
                    <div class="currency-sec-head">
                        Confirm & Place the order
                    </div>
                    <div class="currency-sec-text">
                        <p> You can review your details and place the order. We will take care of the rest!
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section-testimonial section-padding">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="header-section text-light text-center pt-4">
                    <h2 class="text-light">
                        Read our Customer Reviews
                    </h2>
                </div>

            </div>
        </div>
        <div class="row justify-content-center pt-5">
            <div class="col-md-10 col-lg-8">
                <!-- <div class="items-testimonial">
                    <div class="item-testimonial">
                        <div class="item-testimonial-content">
                            “We have chosen the right solution for our companies travel money needs, Aman Finance takes
                            out the hassel in travel money exchange”
                        </div>
                        <div class="item-testimonial-img">
                            <img src="<?php bloginfo("template_directory"); ?>/img/user.png" alt="">
                            <div class="user-info">
                                <div class="user-position">Head of Operations</div>
                                <div class="user-company">
                                    Acme Group
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item-testimonial">
                        <div class="item-testimonial-content">
                            “We have chosen the right solution for our companies travel money needs, Aman Finance takes
                            out the hassel in travel money exchange”
                        </div>
                        <div class="item-testimonial-img">
                            <img src="<?php bloginfo("template_directory"); ?>/img/user.png" alt="">
                            <div class="user-info">
                                <div class="user-position">Head of Operations</div>
                                <div class="user-company">
                                    Acme Group
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
                <?php echo do_shortcode('[google-review-slick]'); ?>

            </div>
        </div>
    </div>
</div>
<div class="footer section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="header-section left pt-4">
                    <h2>Talk to the Currency Expert!
                    </h2>
                </div>
                <div class="contect-info">
                    <div class="tel">
                        <i class="fas fa-phone"></i>
                        <span> 0127 4721 311 </span>
                    </div>
                    <div class="address">
                        Cash & Cheque Direct,
                        47 Darley Street,
                        Bradford,
                        BD1 3HN
                    </div>
                    <div class="email">
                        <a href="mailto:orders@cashandchequedirect.co.uk ">orders@cashandchequedirect.co.uk</a>
                    </div>
                </div>


            </div>
            <div class="col-md-6">
                <!-- <div id="map_canvas"></div> -->
                <!-- <div id="map-container-google-1" class="z-depth-1-half map-container" style="height: 100%">
                    <iframe
                        src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d98876.19601802684!2d-1.8065777466263488!3d53.77457047149308!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487be1493fc2a66d%3A0x4c97c3f780c5ae6b!2sFirst+Choice+Currency!5e0!3m2!1sen!2slk!4v1552037425438"
                        frameborder="0" style="border:0" allowfullscreen></iframe>

                </div> -->
                <?php 
                echo do_shortcode('[responsive_google_map]');
                ?>

            </div>

        </div>
    </div>
</div>
<?php get_footer(); ?>