<?php
/**
 * Understrap functions and definitions
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$understrap_includes = array(
	'/theme-settings.php',                  // Initialize theme default settings.
	'/setup.php',                           // Theme setup and custom theme supports.
	'/widgets.php',                         // Register widget area.
	'/enqueue.php',                         // Enqueue scripts and styles.
	'/template-tags.php',                   // Custom template tags for this theme.
	'/pagination.php',                      // Custom pagination for this theme.
	'/hooks.php',                           // Custom hooks.
	'/extras.php',                          // Custom functions that act independently of the theme templates.
	'/customizer.php',                      // Customizer additions.
	'/custom-comments.php',                 // Custom Comments file.
	'/jetpack.php',                         // Load Jetpack compatibility file.
	'/class-wp-bootstrap-navwalker.php',    // Load custom WordPress nav walker.
	'/woocommerce.php',                     // Load WooCommerce functions.
	'/editor.php',                          // Load Editor functions.
	'/deprecated.php',                      // Load deprecated functions.
	'/google-review.php',                   // Load google review by plugin
);

foreach ( $understrap_includes as $file ) {
	$filepath = locate_template( 'inc' . $file );
	if ( ! $filepath ) {
		trigger_error( sprintf( 'Error locating /inc%s for inclusion', $file ), E_USER_ERROR );
	}
	require_once $filepath;
}

function register_footer_menu() {
	register_nav_menu('footer-menu',__( 'Footer Menu' ));
  }
  add_action( 'init', 'register_footer_menu' );

  /**
* Add a custom link to the end of a specific menu that uses the wp_nav_menu() function
*/
add_filter('wp_nav_menu_items', 'add_admin_link', 10, 2);
function add_admin_link($items, $args){
    if( $args->theme_location == 'primary' ){
		
		$items .= '<li class="nav-item dropdown border-custom mr-2 mb-sm-2">
		<a class="nav-link dropdown" href="#" id="navbarDropdown" role="button"
			data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<i class="fas fa-user"></i>
		</a>
		<div class="dropdown-menu dropdown-menu-md-right" aria-labelledby="navbarDropdown">' ;
		if ( is_user_logged_in()) { 
			$items .='<a class="dropdown-item"
				href='.  get_permalink( get_option('woocommerce_myaccount_page_id') ).'">Account</a>
			<a class="dropdown-item"
				href="'. wp_logout_url( get_permalink( woocommerce_get_page_id( 'myaccount' ) ) ).'">Logout</a>';
		}else {
			$items .='<a class="dropdown-item" href="'. get_permalink( woocommerce_get_page_id( 'myaccount' ) ).'">Login
    / Register</a>';

}
$items .='</div>
</li>';

$items .= '<li class="nav-item dropdown border-custom">
		<a class="nav-link " title="" href="'. wc_get_cart_url() .'">
		<i class="fas fa-shopping-cart"></i>
		</a>
		</li>';
?>

<?php
    }
    return $items;
}