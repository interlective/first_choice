<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
        integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>



    <!-- ******************* The Navbar Area ******************* -->
    <div id="wrapper-navbar" itemscope itemtype="http://schema.org/WebSite">

        <a class="skip-link sr-only sr-only-focusable"
            href="#content"><?php esc_html_e( 'Skip to content', 'understrap' ); ?></a>
        <div class="first_change-menubar">
            <nav class="navbar navbar-expand-md navbar-light">

                <?php if ( 'container' == $container ) : ?>
                <div class="container">
                    <?php endif; ?>

                    <!-- Your site title as branding in the menu -->
                    <?php if ( ! has_custom_logo() ) { ?>

                    <?php if ( is_front_page() && is_home() ) : ?>

                    <h1 class="navbar-brand mb-0"><a rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>"
                            title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"
                            itemprop="url"><?php bloginfo( 'name' ); ?></a></h1>

                    <?php else : ?>

                    <a class="navbar-brand" rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>"
                        title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"
                        itemprop="url"><?php bloginfo( 'name' ); ?></a>

                    <?php endif; ?>


                    <?php } else {
						the_custom_logo();
					} ?>
                    <!-- end custom logo -->

                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
                        aria-controls="navbarNavDropdown" aria-expanded="false"
                        aria-label="<?php esc_attr_e( 'Toggle navigation', 'understrap' ); ?>">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <!-- The WordPress Menu goes here -->
                    <?php wp_nav_menu(
					array(
						'theme_location'  => 'primary',
						'container_class' => 'collapse navbar-collapse',
						'container_id'    => 'navbarNavDropdown',
						'menu_class'      => 'navbar-nav ml-auto',
						'fallback_cb'     => '',
						'menu_id'         => 'main-menu',
						'depth'           => 2,
						'walker'          => new Understrap_WP_Bootstrap_Navwalker(),
					)
                ); ?>
                    <!-- <ul class="nav navbar-nav navbar-right cart-myacount-wrapper"> -->
                    <!-- <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-user"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-md-right" aria-labelledby="navbarDropdown">
                                <?php if( is_user_logged_in()) : ?>
                                <a class="dropdown-item"
                                    href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>">Account</a>
                                <a class="dropdown-item"
                                    href="<?php echo wp_logout_url( get_permalink( woocommerce_get_page_id( 'myaccount' ) ) );?>">Logout</a>
                                <?php else : ?>
                                <a class="dropdown-item"
                                    href="<?php echo  get_permalink( woocommerce_get_page_id( 'myaccount' ) );?>">Login
                                    / Register</a>
                                <?php endif; ?>
                            </div>
                        </li> -->
                    <!-- 
                        <li class="nav-item dropdown">
                            <a class="nav-link " href="<?php echo wc_get_cart_url(); ?>">
                                <i class="fas fa-shopping-cart"></i>
                            </a> -->
                    <!-- <div class="dropdown-menu  dropdown-menu-md-right " aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="">View Cart</a>
                                <div class="dropdown-item" href="#">
                                
                                </div>
                            </div> -->
                    <!-- <ul class="dropdown-menu  dropdown-menu-md-right dropdown-cart" role="menu">
                                <?php
    //                              global $woocommerce;
    // $items = $woocommerce->cart->get_cart(); 
    // foreach($items as $item => $values) { 
    //     $_product =  wc_get_product( $values['data']->get_id());
    ?>
                                <li>
                                    <div class="item">
                                        <div class="item-left">

                                            <div class="item-info">
                                                <div><?php // echo $_product->get_title(); ?></div>
                                                <div>Amount : <?php //echo $values['quantity'] ?></div>
                                            </div>
                                        </div>
                                        <div class="item-close">
                                            <a href="<?php // WC()->cart->remove_cart_item($cart_item_key); ?>"> <button class="fa fa-close btn-close border-0"></button></a>
                                        </div>
                                    </div>
                                </li>

                                <?php // } ?>

                                <li class="divider"></li>
                                <li><a class="text-center" href="<?php //echo wc_get_cart_url(); ?>">View Cart</a></li>
                            </ul> -->
                    <!-- </li>


                    </ul> -->
                    <?php if ( 'container' == $container ) : ?>
                </div><!-- .container -->
                <?php endif; ?>

            </nav><!-- .site-navigation -->


        </div>
    </div><!-- #wrapper-navbar end -->