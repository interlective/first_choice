<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package understrap
 */

?>

<div id="accordion">

    <div class="card">
        <div class="card-header">
            <a class="card-link" data-toggle="collapse" href="#collapseOne">
                What is click and collect?
            </a>
        </div>
        <div id="collapseOne" class="collapse show" data-parent="#accordion">
            <div class="card-body">
                Our click and collect service allows customers to order currency online and then collect in selected
                stores.
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
                When will my order be ready collect?
            </a>
        </div>
        <div id="collapseTwo" class="collapse" data-parent="#accordion">
            <div class="card-body">
                Please see our table which details a weekly display or order days and respective collection days.
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">
                Do you charge commission?
            </a>
        </div>
        <div id="collapseThree" class="collapse" data-parent="#accordion">
            <div class="card-body">
                No. We are absolutely commission free, with no hidden cost.
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <a class="collapsed card-link" data-toggle="collapse" href="#collapsefour">
                Do you charge a card handling fee?
            </a>
        </div>
        <div id="collapsefour" class="collapse" data-parent="#accordion">
            <div class="card-body">
                You will not be charged a card processing fee if your method of payment is debit card. Unfortunately, we
                do not accept any credit cards.
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <a class="collapsed card-link" data-toggle="collapse" href="#collapsefive">
                What is the minimum/maximum currency I can order?
            </a>
        </div>
        <div id="collapsefive" class="collapse" data-parent="#accordion">
            <div class="card-body">
                There is no minimum order value on our click and collect service, however each individual is only
                allowed to order a maximum or £2,499.99 per day through the click and collect service.
            </div>
        </div>
    </div>


</div>