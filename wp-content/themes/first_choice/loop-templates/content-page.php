<?php
/**
 * Partial template for content in page.php
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>



<div class="col-12 text-center mb-3">
    <div class="header-section  pt-4 ">
        <?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>
    </div>
</div>


<div class="entry-content col-md-8">

    <?php the_content(); ?>


</div><!-- .entry-content -->
<?php
		wp_link_pages(
			array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'understrap' ),
				'after'  => '</div>',
			)
		);
		?>

<footer class="entry-footer">

    <?php edit_post_link( __( 'Edit', 'understrap' ), '<span class="edit-link">', '</span>' ); ?>

</footer><!-- .entry-footer -->