<?php
/**
 * Partial template for content in page.php
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
<div class="col-12 text-center mb-3">
    <div class="header-section pt-4 mb-3">
        <?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>
    </div>
</div>


<div class="col-12">
    <div class="map-wraper mb-4">
        <?php 
                echo do_shortcode('[responsive_google_map]');
                ?>

    </div>

</div>
<div class="col-md-4">
    <div class="contect-info">
        <div class="tel mb-4">
            <i class="fas fa-phone"></i>
            <span> 01274 721 311</span>
        </div>
        <div class="address">
            <strong class="mb-3">
                STORE LOCATION
            </strong>
            <div class="add-item">
                Cash & Cheque Direct<br />
                47 Darley Street<br />
                Bradford<br />
                BD1 3HN.<br />
            </div>
            <div class="add-item">
                Cash & Cheque Direct<br />
                13 Northgate<br />
                Halifax<br />
                HX1 1RU.<br />
            </div>
            <div class="add-item">
                First Choice Currency <br />
                34 Market Street<br />
                Bradford<br />
                BD1 1NF.<br />
            </div>
        </div>
        <div class="email">
            <a href="mailto:orders@cashandchequedirect.co.uk">orders@cashandchequedirect.co.uk</a>
        </div>
    </div>

</div>
<div class="col-md-8 ">
    <?php the_content(); ?>
</div>

<div class="entry-content">



    <?php
		wp_link_pages(
			array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'understrap' ),
				'after'  => '</div>',
			)
		);
		?>

</div><!-- .entry-content -->

<footer class="entry-footer">

    <?php edit_post_link( __( 'Edit', 'understrap' ), '<span class="edit-link">', '</span>' ); ?>

</footer><!-- .entry-footer -->