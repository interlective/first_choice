<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'understrap_container_type' );
?>

<div class="footer-menu-sec mt-4">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="footer-menu menu-link">
                    <?php wp_nav_menu( array( 'theme_location' => 'footer-menu', 'menu_class' => 'list-inline mb-0' ) ); ?>

                </div>
                <div class="social-link">
                    <ul class="list-inline mb-0">
                        <li class="list-inline-item"> <a href="#"> <i class="fab fa-facebook-square"></i></a></li>
                        <li class="list-inline-item"><a href="#"><i class="fab fa-twitter-square"></i></a></li>
                        <li class="list-inline-item"><a href="#"><i class="fab fa-instagram"></i></a></li>
                        <li class="list-inline-item"><a href="#"><i class="fab fa-linkedin-square"></i></a></li>

                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <div class="copyright text-right">
                    <p>Copyright © 2019 All rights reserved.
                        <br />
                        Developed By <a href="https://www.vinitsolutions.com/" target="_blank"> Vinit Solution</a></p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script> -->
<script>

</script>

<?php wp_footer(); ?>

</body>

</html>