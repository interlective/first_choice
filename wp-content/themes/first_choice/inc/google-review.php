<?php 
add_shortcode("google-review-slick","google_review");

function google_review(){
	
		global $wpdb;
	
    	//use values from currentform to get reviews from db
		$table_name = $wpdb->prefix . 'wpfb_reviews';
		$rlength = 1;
		$sorttable = "created_time_stamp ";
		$sortdir = "DESC";
		$rtype = "Google";
		$reviewsperpage= 5;
		$tablelimit = $reviewsperpage;
		
        
       // var_dump($table_name);

		$totalreviews = $wpdb->get_results(
			$wpdb->prepare("SELECT * FROM ".$table_name."
			WHERE id>%d AND review_length >= %d AND type = %s
			ORDER BY ".$sorttable." ".$sortdir." 
			LIMIT ".$tablelimit." ", "0","$rlength","$rtype")
		);
     	//var_dump($totalreviews);
	// 		echo "<br><br>";

	ob_start();
?>
<div class="items-testimonial">
    <?php foreach($totalreviews as $_result){ ?>
    <div class="item-testimonial">

        <div class="item-testimonial-img">
            <img src="<?php echo $_result->userpic; ?>" alt="">
            <div class="user-info">
                <div class="user-position"><?php echo $_result->reviewer_name; ?></div>
                <div class="user-company">

                    <?php
					$_count = $_result->rating;
					for ($x = 1; $x <= $_count; $x++) {
					 ?>
                    <span class="fa fa-star checked"></span>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="item-testimonial-content">
            <?php echo $_result->review_text; ?>

        </div>
    </div>
    <?php } ?>

</div>




<?php
 $out_put = ob_get_contents();
 ob_end_clean();
 return $out_put;
 wp_reset_postdata();
}

?>