<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();

$container   = get_theme_mod( 'understrap_container_type' );

?>

<div class="wrapper page-faq" id="page-wrapper">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center mb-3">
                <div class="header-section  pt-4 ">
                    <?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>
                </div>
            </div>
            <div class="col-md-8">

                <?php while ( have_posts() ) : the_post(); ?>

                <?php get_template_part( 'loop-templates/content', 'faq' ); ?>

                <?php
		// If comments are open or we have at least one comment, load up the comment template.
		if ( comments_open() || get_comments_number() ) :
			comments_template();
		endif;
		?>

                <?php endwhile; // end of the loop. ?>


            </div>
        </div>
    </div>

</div><!-- Wrapper end -->

<?php get_footer(); ?>