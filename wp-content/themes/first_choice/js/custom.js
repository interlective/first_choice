(function ($) {

    $('.items-testimonial').slick({
        autoplay: false,
        autoplaySpeed: 3000,
        dots: true,
        arrows: false,
        adaptiveHeight: true
    });
})(jQuery);